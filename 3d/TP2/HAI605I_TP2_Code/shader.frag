// --------------------------------------------------------------------------
// gMini,
// a minimal Glut/OpenGL app to extend                              
//
// Copyright(C) 2007-2009                
// Tamy Boubekeur
//                                                                            
// All rights reserved.                                                       
//                                                                            
// This program is free software; you can redistribute it and/or modify       
// it under the terms of the GNU General Public License as published by       
// the Free Software Foundation; either version 2 of the License, or          
// (at your option) any later version.                                        
//                                                                            
// This program is distributed in the hope that it will be useful,            
// but WITHOUT ANY WARRANTY; without even the implied warranty of             
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
// GNU General Public License (http://www.gnu.org/licenses/gpl.txt)           
// for more details.                                                          
//                                                                          
// --------------------------------------------------------------------------

uniform float ambientRef;
uniform float diffuseRef;
uniform float specRef;
uniform float shininess;

varying vec4 p;
varying vec3 n;

void main (void) {
     vec3 P = vec3 (gl_ModelViewMatrix * p); //Position du point à éclairer
     vec3 N = normalize (gl_NormalMatrix * n); //Normal en ce point
     vec3 V = normalize (-P); //Vecteur de vue (était à -P de base)

     // calcul de la couleur ambiante
    vec4 lightContribution = ambientRef * gl_LightModel.ambient*gl_FrontMaterial.ambient;

    for (int i = 0; i<3; i++){
        vec3 L = normalize (gl_LightSource[i].position.xyz - P);
        float diffuse = max (dot (L, N), 0.0);
        vec3 R = reflect (-L, N);


        float spec = max(dot(R, V), 0.0);
        spec = pow(spec, shininess);
        spec = max (0.0, spec);


        lightContribution += diffuseRef * diffuse * gl_LightSource[i].diffuse * gl_FrontMaterial.diffuse + specRef * spec * gl_LightSource[i].specular * gl_FrontMaterial.specular;
    }


    // autre façon

    //on calcul l'angle nécessaire au calcul de la lumière diffuse (cf loi de lambert)
    //vec3 rayon = normalize(gl_LightSource[0].position.xyz-P);
    //float angle = dot(N, rayon);

    // calcul de la composante diffuse
    //lightContribution += diffuseRef * gl_LightSource[0].diffuse * gl_FrontMaterial.diffuse * angle;

    // seulement quand l'angle est supérieur à 0 sinon 
    //if (angle > 0.0f){
        // calcul de la composante spéculaire
         //lightContribution += specRef * gl_LightSource[0].specular * gl_FrontMaterial.specular * pow(dot(N, V-rayon), shininess);
    //}




    ////////////////////////////////////////////////
    //Eclairage de Phong à calculer en utilisant
    ///////////////////////////////////////////////
    // gl_LightSource[i].position.xyz Position de la lumière i
    // gl_LightSource[i].diffuse Couleur diffuse de la lumière i
    // gl_LightSource[i].specular Couleur speculaire de la lumière i
    // gl_FrontMaterial.diffuse Matériaux diffus de l'objet
    // gl_FrontMaterial.specular Matériaux speculaire de l'objet

    gl_FragColor =vec4 (lightContribution.xyz, 1);
}
 
