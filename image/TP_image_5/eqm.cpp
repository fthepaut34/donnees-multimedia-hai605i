#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgLue2[250];
  int nH, nW, nTaille;
  int eqm;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageIn2.pgm \n"); 
       exit (1);
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgLue2);

   OCTET *ImgIn, *ImgIn2;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue2, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);

   for(int i=0;i<nH;i++){
    for(int j=0;j<nW;j++){
      eqm += (ImgIn[i*nW+j] - ImgIn2[i*nW+j])*(ImgIn[i*nW+j] - ImgIn2[i*nW+j]);
    }
   }

   eqm = eqm/nTaille;

   printf("L'erreur quadratique moyenne entre les deux images est de : %i\n", eqm);

   free(ImgIn);
   free(ImgIn2);
   return 1;
}