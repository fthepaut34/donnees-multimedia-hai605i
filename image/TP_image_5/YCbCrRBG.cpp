#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "image_ppm.h"

using namespace std;

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgLue2[250], cNomImgLue3[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn.pgm ImageIn2.pgm ImageIn3.pgm ImageOut.ppm \n"); 
       exit (1);
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgLue2);
   sscanf (argv[3],"%s",cNomImgLue3);
   sscanf (argv[4],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgIn2, *ImgIn3, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   //lire_nb_lignes_colonnes_image_pgm(cNomImgLue2, &nH, &nW);
   //lire_nb_lignes_colonnes_image_pgm(cNomImgLue3, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   allocation_tableau(ImgIn3, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);
   lire_image_pgm(cNomImgLue3, ImgIn3, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);

   for(int i=0;i<nTaille3;i+=3){
    nR = ImgIn[i/3] + 1.402 * (ImgIn3[i/3] - 128);
    nG = ImgIn[i/3] - 0.34414 * (ImgIn2[i/3] - 128) - 0.714414 * (ImgIn3[i/3] - 128);
    nB = ImgIn[i/3] + 1.772 * (ImgIn2[i/3] - 128);
    ImgOut[i] = min(max(0,nR),255);
    ImgOut[i+1] = min(max(0,nB),255);
    ImgOut[i+2] = min(max(0,nG),255);
   }

   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);
   free(ImgIn2);
   free(ImgIn3);
   free(ImgOut);
   return 1;
}