#include <stdio.h>
#include <iostream>
#include "image_ppm.h"

using namespace std;

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB;
  int k;
  
  if (argc != 4)
  {
    printf("Usage: ImageIn.pgm ImageOut.pgm k\n"); 
    exit(1);
  }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%i",&k);

   if ((k<(-128)) || (k>128))
  {
    printf("Usage: k doit être compris entre -128 et 128\n");
    exit(1);
  }

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   for (int i=0;i<nTaille;i++){
    ImgOut[i] = min(max(ImgIn[i]+k,0),255);
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);
   free(ImgOut);
   return 1;
}