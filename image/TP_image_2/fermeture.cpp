#include <stdio.h>
#include "image_ppm.h"

// trop chiant d'intégrer les fichiers donc on recopie erosion et dilatation sous forme de fonction

void dilatation(char* cNomImgLue, char* cNomImgEcrite){
  int nH, nW, nTaille;

   OCTET *ImgIn, *ImgOut;

   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   for (int i=1;i<nH-1; i++){
    for (int j=1;j<nW-1;j++){
      if (ImgIn[(i*nW+j)] == 255){
        ImgOut[i*nW+j] = 255;
        continue;
      }
      bool blanc = false;
      for (int k=(-1);k<2;k++){
        for (int l=(-1);l<2;l++){
          if (ImgIn[(i+k)*nW+(j+l)] == 255){
            blanc = true;
          }
          if (blanc)
            ImgOut[i*nW+j] = 255;
          else
            ImgOut[i*nW+j] = 0;
        }
      }
    }
  }
  ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
  free(ImgIn); free(ImgOut);
}








void erosion(char* cNomImgLue, char* cNomImgEcrite){
  int nH, nW, nTaille;
  OCTET *ImgIn, *ImgOut;


  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  
  allocation_tableau(ImgIn, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
  allocation_tableau(ImgOut, OCTET, nTaille);

  for (int i=1;i<nH-1; i++){
    for (int j=1;j<nW-1;j++){
      if (ImgIn[(i*nW+j)] == 0){
        ImgOut[i*nW+j] = 0;
        continue;
      }
      bool noir = false;
      for (int k=(-1);k<2;k++){
        for (int l=(-1);l<2;l++){
          if (ImgIn[(i+k)*nW+(j+l)] == 0){
            noir = true;
          }
          if (noir)
            ImgOut[i*nW+j] = 0;
          else
            ImgOut[i*nW+j] = 255;
        }
      }
    }
  }

  ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
  free(ImgIn); free(ImgOut);
}







int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250], cNomImgEcrite2[250];
  int nH, nW, nTaille;
  
  if (argc != 4)
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm ImageOut2.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue);
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%s",cNomImgEcrite2);

   dilatation(cNomImgLue, cNomImgEcrite);
   erosion(cNomImgEcrite, cNomImgEcrite2);

   return 1;
}