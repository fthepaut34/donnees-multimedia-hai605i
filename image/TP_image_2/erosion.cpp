#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;

   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   for (int i=1;i<nH-1; i++){
    for (int j=1;j<nW-1;j++){
      if (ImgIn[(i*nW+j)] == 0){
        ImgOut[i*nW+j] = 0;
        continue;
      }
      bool noir = false;
      for (int k=(-1);k<2;k++){
        for (int l=(-1);l<2;l++){
          if (ImgIn[(i+k)*nW+(j+l)] == 0){
            noir = true;
          }
          if (noir)
            ImgOut[i*nW+j] = 0;
          else
            ImgOut[i*nW+j] = 255;
        }
      }
    }
  }

  ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
  free(ImgIn); free(ImgOut);

  return 1;
}

// on part du principe que les points d'intérets sont en blancs de base donc erosion = supprimer les pixels qui ne font pas partie de l'objet
// dilatation = supprimer les pixels qui font partie de l'objet