#include <stdio.h>
#include "image_ppm.h"


void dilatation_grey(char* cNomImgLue, char* cNomImgEcrite){
  int nH, nW, nTaille;

   OCTET *ImgIn, *ImgOut;

   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   for (int i=1;i<nH-1; i++){
    for (int j=1;j<nW-1;j++){
      int max = ImgIn[(i+1)*nW+j]; // on initialise le max avec un voisin quelconque
      for (int k=(-1);k<2;k++){
        for (int l=(-1);l<2;l++){
          if (max<ImgIn[(i+k)*nW+(j+l)]){
            max = ImgIn[(i+k)*nW+(j+l)];
          }
        }
      }
      ImgOut[i*nW+j] = max;
    }
  }

  ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
  free(ImgIn); free(ImgOut);
}










void erosion_grey(char* cNomImgLue, char* cNomImgEcrite){
  int nH, nW, nTaille;

  OCTET *ImgIn, *ImgOut;

  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  
  allocation_tableau(ImgIn, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
  allocation_tableau(ImgOut, OCTET, nTaille);

  for (int i=1;i<nH-1; i++){
    for (int j=1;j<nW-1;j++){
      int min = ImgIn[(i+1)*nW+j]; // on initialise le min avec un voisin quelconque
      for (int k=(-1);k<2;k++){
        for (int l=(-1);l<2;l++){
          if (min>ImgIn[(i+k)*nW+(j+l)]){
            min = ImgIn[(i+k)*nW+(j+l)];
          }
        }
      }
      ImgOut[i*nW+j] = min;
    }
  }

  ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
  free(ImgIn); free(ImgOut);
}







int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250], cNomImgEcrite2[250];
  int nH, nW, nTaille;
  
  if (argc != 4)
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm ImageOut2.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue);
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%s",cNomImgEcrite2);

   dilatation_grey(cNomImgLue, cNomImgEcrite);
   erosion_grey(cNomImgEcrite, cNomImgEcrite2);

   return 1;
}