#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgLue2[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  int nbPFD = 0;
  float surface;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageIn2.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue);
   sscanf (argv[2],"%s",cNomImgLue2);
   sscanf (argv[3],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgIn2, *ImgOut;

   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);


   for (int i =0;i<nH;i++){
    for (int j=0;j<nW;j++){
      if ((ImgIn[i*nW+j] == 255) && (ImgIn2[i*nW+j] == 0)){
        ImgOut[i*nW+j] = 0;
        nbPFD++;
      }
      else{
        ImgOut[i*nW+j] = 255;
      }
    }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
   free(ImgIn); free(ImgIn2); free(ImgOut);
   printf("nbPFD : %d\n", nbPFD);
   surface = nbPFD*(0.25*0.25);
   printf ("La surface de forêt détruite entre 2000 et 2012 est de %f km².\n", surface);

   return 1;
 }