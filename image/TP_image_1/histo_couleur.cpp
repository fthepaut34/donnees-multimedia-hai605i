#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250];
  int nH, nW, nTaille;
  int N = 256;
  
  if (argc != 2)
     {
       printf("Usage: ImageIn.pgm \n");
       exit (1);
     }
   
  sscanf (argv[1],"%s",cNomImgLue);

  OCTET *ImgIn;
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgIn, OCTET, nTaille3);
  lire_image_ppm(cNomImgLue, ImgIn, nH * nW);

  int T[N][4];

  for (int i = 0; i<256; i++){
    T[i][0] = i;
    T[i][1] = 0;
    T[i][2] = 0;
    T[i][3] = 0;
  }

  for (int i = 0; i< nTaille3; i+=3){
      T[ImgIn[i]][1]++;
      T[ImgIn[i+1]][2]++;
      T[ImgIn[i+2]][3]++;
    }

  std::string fichier = "/home/e20180008947/Bureau/Données Multimédia/donnees-multimedia-hai605i/TP_image_1/histo_couleur.dat";
  std::ofstream flux(fichier.c_str());
  
  for (int i = 0; i<N; i++){
    std::cout<<T[i][0]<<" : "<<T[i][1] << " : " << T[i][2] << " : " << T[i][3] << "\n";
    if(flux){
      flux << T[i][0] << "\t" << T[i][1] << "\t" << T[i][2] << "\t" << T[i][3] << "\n";
    }
  }

  flux.close();

  //replot "histo_couleur.dat" using 1:4 with lines
  
  return 1;
  
}
