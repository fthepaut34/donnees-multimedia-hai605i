#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"

int main(int argc, char* argv[]){
  char cNomImgLue[250];
  int indice;
  int nH, nW, nTaille;
  int N = 256;

  if(argc != 3){
    printf("Usage: ImageIn.pgm indice \n");
    exit (1);
  }

  sscanf (argv[1],"%s",cNomImgLue);
  sscanf (argv[2],"%d",&indice);

  OCTET *ImgIn;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgIn, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

  
  int T[nW][2];
  for (int i = 0; i<nW; i++){
    T[i][0] = i;
    T[i][1] = 0;
  }
  for (int i = 0; i<nW; i++)
    T[i][1] = ImgIn[indice*nW+i];

  std::string fichier = "/home/e20180008947/Bureau/Données Multimédia/donnees-multimedia-hai605i/TP_image_3/profil.dat";
  std::ofstream flux(fichier.c_str());

  for (int i = 0; i<nW; i++){
    std::cout<<T[i][0]<<" : "<<T[i][1]<<"\n";
    if(flux){
      flux << T[i][0] << "\t" << T[i][1] << "\n";
    }
  }

  flux.close();

  return 1;
  
}
