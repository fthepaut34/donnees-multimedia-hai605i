// test_couleur.cpp : Seuille une image en niveau de gris en 3 parties

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  int N = 256;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);


  float T[N][2];

  for (int i = 0; i<256; i++){
    T[i][0] = i;
    T[i][1] = 0;
  }
  
  for (int i = 0; i<nH; i++){
    for (int j = 0; j<nW; j++){
      T[ImgIn[i*nW+j]][1]++;
    }
  }

  T[0][1] = T[0][1]/(nH*nW);

  for (int i = 1; i<256; i++){
    T[i][1] = T[i-1][1] + T[i][1]/(nH*nW);
  }

  for (int i=0; i < nH; i++){
    for (int j=0; j < nW; j++){
      ImgOut[i*nW+j] = T[ImgIn[i*nW+j]][1]*255;
      
    }
  }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
